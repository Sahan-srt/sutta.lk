var menustat = 0;
var ele = document.querySelector('.menu').querySelectorAll('li');
var eletotal = ele.length;
while (eletotal--) {
    ele[eletotal].addEventListener('click', function(e) {
        this.querySelectorAll('ul')[0].style.display = "block";
    });
}

function openNav() {
    if (menustat === 0){
        document.querySelector(".col-md-9").style.paddingLeft = "28em";
        document.querySelector(".col-md-3").style.width = "26em";
        document.querySelector(".col-md-3").style.borderColor = "#e3d8d4";
        menustat = 1;
    }else{
        document.querySelector(".col-md-3").style.width = "0";
        document.querySelector(".col-md-3").style.borderColor = "#f2f2f2";
        document.querySelector(".col-md-9").style.paddingLeft = "5%";
        menustat = 0;
    }
}